package org.webrtc.webrtcdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class LauncherActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(new View(this));
		startActivity(new Intent(this, WebRTCDemo.class));
		this.finish();
	}
}
